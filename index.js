'use strict';
const client = require('prom-client');

const http = require('http');

module.exports = function (seneca, opts) {
    let options = Object.assign({
        port: 9615
    }, opts);

    http.createServer(function (req, res) {
      res.writeHead(200, {'Content-Type': 'text/plain'});
      res.end(client.register.metrics());
    }).listen(options.port);

    const request_timing = new client.Summary('seneca_request_duration_seconds', 'Время запроса', ['method', 'status']);
    const request_count = new client.Counter('seneca_requests', 'Запросы seneca', ['method', 'status']);

    console.log(client.defaultMetrics.metricsList);


    seneca.decorate('addProm', (method, callback) => {

        seneca.add(method, (data, reply) => {
            const start = process.hrtime();

            callback(data, (err, result) => {
                const time = process.hrtime(start);
                request_timing.labels(JSON.stringify(method), err ? err.status : 200).observe(time[0] + time[1] / 100000000);
                request_count.labels(JSON.stringify(method), err ? err.status : 200).inc();

                return reply(err, result);
            });
        });
    });
};
